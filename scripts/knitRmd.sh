#! /bin/bash

for var in "$@"
do
    # Render rmarkdown document
    Rscript -e "rmarkdown::render('$var')"
    # Remove temporary folder
    folder=${var%.Rmd}_files
    rm -fr $folder
done

