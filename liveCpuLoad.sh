#! /bin/bash

nChar=4
idle=`mpstat 1 1 | tail -n+5 | tr -s " " | cut -d" " -f 12`
load=`echo "100 - $idle" | bc -l`
nLoad=`echo "$load/100 * $nChar" | bc -l`
nIdle=`echo "$nChar - $nLoad" | bc -l`
# https://unix.stackexchange.com/questions/101252/how-to-round-or-convert-a-float-value-to-int-with-bc-getting-standard-in-1
nLoadInt=`printf "%.0f" $nLoad`
nIdleInt=`echo "$nChar - $nLoadInt" | bc -l`

# https://stackoverflow.com/questions/5799303/print-a-character-repeatedly-in-bash
echo -n "`head -c $nLoadInt /dev/zero | tr '\0' \"*\"``head -c $nIdleInt /dev/zero | tr '\0' \".\"`"

