;;; ess-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "ess" "ess.el" (24056 37601 0 0))
;;; Generated autoloads from ess.el

(autoload 'ess-version "ess" "\
Return a string with ESS version information.

\(fn)" t nil)

(autoload 'ess-submit-bug-report "ess" "\
Submit a bug report to the ESS maintainers.

\(fn)" t nil)

;;;***

(provide 'ess-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ess-autoloads.el ends here
