(define-package "julia-mode" "20191207.718" "Major mode for editing Julia source code" 'nil :keywords
  '("languages")
  :url "https://github.com/JuliaLang/julia")
;; Local Variables:
;; no-byte-compile: t
;; End:
