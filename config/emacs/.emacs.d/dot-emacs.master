;;; -*- mode: lisp -*-
;;; * Description
;;; dot-emacs.master file
;;; master .emacs file, loading other .emacs files as needed
;;; You can add to your .bashrc file:
;;; alias emacs='emacs -nw -l ~/org/computer/emacs-config/dot-emacs.master'

(package-initialize)
(add-to-list 'load-path "~/.emacs.d/elpa")

;;; * Useful resources
;;; Sacha Chua's dotemacs: http://pages.sachachua.com/.emacs.d/Sacha.html#org332b2fd
;;; AAron Bedra: http://pages.sachachua.com/.emacs.d/Sacha.html#org332b2fd
;;; doc.norang.ca

;;; * Notes about autoloads

;;; To avoid messages like:
;;; Error loading autoloads: (file-error Cannot open load file No such file or directory /home/matthieu/.emacs.d/elpa/polymode-20190714.2017/polymode-autoloads)
;;; I run once via M-X eval-expr:
;;; (update-file-autoloads "~/.emacs.d/elpa/polymode-20190714.2017/polymode.el" t "~/.emacs.d/elpa/polymode-20190714.2017/polymode-autoloads.el")
;;; Cf.:
;;; https://emacs.stackexchange.com/questions/8043/how-to-use-function-update-file-autoloads-and-and-variable-generated-autoload-fi

;;; * Packages to install by hand (with M-X list-packages)
;;; magit
;;; ess
;;; markdown-mode
;;; w3m
;;; polymode
;;; yaml

;;; * User details

(setq user-full-name "Matthieu Bruneaux")
(setq user-mail-address "matthieu.bruneaux@gmail.com")

;;; * Require common lisp

(require 'cl)

;;; * gnutls

;;; https://github.com/vermiculus/sx.el/issues/283
;;; https://www.emacswiki.org/emacs/rcirc
(setq gnutls-min-prime-bits 1024)

;;; * Repositories

;;(load "package")
;;(package-initialize)
;;; https://www.emacswiki.org/emacs/ELPA
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
			 ("marmalade" . "https://marmalade-repo.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")))

;;; * Themes

(add-to-list 'custom-theme-load-path "~/.emacs.d/emacs-themes/")
(load-theme 'zenburn-MB t)

;;; * Support for ANSI colors

(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;;; * Other

;;; https://www.emacswiki.org/emacs/SwitchingBuffers
(global-set-key (kbd "M-s")  'mode-line-other-buffer)

;;; Do not save abbreviations
;;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Abbrevs.html
(setq save-abbrevs nil)

;;; * General configuration

;;; ** Start-up options

(setq inhibit-splash-screen t
      initial-scratch-message nil)

;;; ** No tabs for indentation
(setq-default indent-tabs-mode nil)

;;; ** Completion buffer sorted vertically
;;; https://emacs.stackexchange.com/questions/27675/change-completions-list-to-sort-vertically
(setq-default completions-format 'vertical)

;;; * Save history

;;; https://stackoverflow.com/questions/1229142/how-can-i-save-my-mini-buffer-history-in-emacs
(setq savehist-file "~/.emacs.d/.emacs.savehist")
(savehist-mode 1)

;;; ** Auto-refresh for modified files

;;; How to have Emacs auto-refresh all buffers when files have changed on disk?
;;; http://stackoverflow.com/questions/1480572/how-to-have-emacs-auto-refresh-all-buffers-when-files-have-changed-on-disk
(global-auto-revert-mode t)

;;; ** Revert all open files

;;; https://emacs.stackexchange.com/questions/24459/revert-all-open-buffers-and-ignore-errors
(defun revall ()
  "Refresh all open file buffers without confirmation.
Buffers in modified (not yet saved) state in emacs will not be reverted. They
will be reverted though if they were modified outside emacs.
Buffers visiting files which do not exist anymore or are no longer readable
will be killed."
  (interactive)
  (dolist (buf (buffer-list))
    (let ((filename (buffer-file-name buf)))
    ;; Revert only buffers containing files, which are not modified;
    ;; do not try to revert non-file buffers like *Messages*.
    (when (and filename (not (buffer-modified-p buf)))
      (if (file-readable-p filename)
	  ;; If the file exists and is readable, revert the buffer
	  (with-current-buffer buf
	    (revert-buffer :ignore-auto :noconfirm :preserve-modes))
	;; Otherse, kill the buffer
	(let (kill-buffer-query-functions) ; No query done when killing buffer
	  (kill-buffer buf)
	  (message "Killed non-existing/readable file buffer: %s" filename))))))
  (message "Finished reverting buffers containing unmodified files."))

;;; ** Display

(setq-default indicate-empty-lines t)
(when (not indicate-empty-lines)
  (toggle-indicate-empty-lines))
(setq column-number-mode t)
(setq line-number-mode t)
(global-linum-mode t) ; line number mode
(setq linum-format "%d ") ; http://superuser.com/questions/346944/display-line-numbers-on-left-margin-in-emacs
;;; see also http://www.emacswiki.org/emacs/LineNumbers#toc7
;;; menu bar
(menu-bar-mode -1)
;;; http://stackoverflow.com/questions/7577614/emacs-truncate-lines-in-all-buffers
(setq-default truncate-lines t)

;;; ** Marking text
(delete-selection-mode t)
(setq x-select-enabe-clipboard t)

;;; ** Kill buffer without confirmation if not modified

;;; http://stackoverflow.com/questions/6467002/how-to-kill-buffer-in-emacs-without-answering-confirmation
(global-set-key (kbd "C-x k") 'kill-this-buffer)

;;; ** Backup files

(setq make-backup-files nil)
;;; Autosave
(setq auto-save-default nil)

;;; ** Yes or no

(defalias 'yes-or-no-p 'y-or-n-p)

;;; ** Other

(setq echo-keystrokes 0.1)
(show-paren-mode t)

;;; ** Set column fill to 79

;;; http://stackoverflow.com/questions/3566727/how-to-set-the-default-width-of-fill-mode-to-80-with-emacs
(setq-default fill-column 79)

;;; ** Set width of buffer name column in Buffer window

(setq Buffer-menu-name-width 50)

;;; ** Undo
(global-set-key (kbd "M-u") 'undo)

;;; * Orgstruct for other modes

;;; https://stackoverflow.com/questions/14941429/structuring-a-statistical-analysis-with-r-using-emacs-ess

;;; ** ESS mode

(add-hook 'ess-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*### ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*### ")
	     ;;(autopair-mode)
	     (setq ess-fancy-comments nil)
	     (ess-toggle-underscore 1)
	     (ess-toggle-underscore nil)
	     ))

;; ;;; ** Markdown mode

;; (add-hook 'markdown-mode-hook
;;           '(lambda ()
;;              (orgstruct-mode)
;; 	     (setq outline-regexp "[ ]*### ")
;; 	     (setq orgstruct-heading-prefix-regexp "[ ]*### ")
;;              (toggle-truncate-lines)
;;              (visual-line-mode)
;;             ))

;;; ** Shell script mode

(add-hook 'sh-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*### ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*### ")
             ))

;;; ** lisp mode

(add-hook 'lisp-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*;;; ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*;;; ")
	     ))

;;; ** Python mode

(add-hook 'python-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*### ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*### ")
	     ))

;;; ** Makefile mode

(add-hook 'makefile-gmake-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*### ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*### ")
	     ))

;;; ** YAML mode

(require 'yaml-mode)
(add-hook 'yaml-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*### ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*### ")
	     ))

;;; ** TeX mode

(add-hook 'latex-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*%%% ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*%%% ")
             (toggle-truncate-lines)
             (visual-line-mode)
	     ))

;;; ** BibTeX mode

(add-hook 'bibtex-mode-hook
          '(lambda ()
             (toggle-truncate-lines)
             (visual-line-mode)
             ))

;;; ** C++ mode

(add-hook 'c++-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*/// ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*/// ")
             (visual-line-mode)
	     ))

;;; ** C mode

(add-hook 'c-mode-hook
	  '(lambda ()
	     (orgstruct-mode)
	     (setq outline-regexp "[ ]*/// ")
	     (setq orgstruct-heading-prefix-regexp "[ ]*/// ")
             (visual-line-mode)
	     ))

;;; * Resizing and navigating

;;; ** Moving cursor faster

;;; https://stackoverflow.com/questions/2657568/how-do-you-move-the-pointer-up-or-down-multiple-lines-with-emacs
(global-set-key (kbd "M-n") (lambda () (interactive) (next-line 5) ))
(global-set-key (kbd "M-p") (lambda () (interactive) (next-line -5) ))

;;; ** Split windows

;;; Keybinding useful when changing terminal size
(defun resizeVert ()
  (interactive)
  (delete-other-windows)
  (split-window-below)
  (other-window 1)
  (mode-line-other-buffer)
  (other-window 1))
(defun resizeHori ()
  (interactive)
  (delete-other-windows)
  (split-window-right)
  (other-window 1)
  (mode-line-other-buffer)
  (other-window 1))
(defun resizeFull()
  (interactive)
  (delete-other-windows))
(global-set-key (kbd "M-2") 'resizeVert)
(global-set-key (kbd "M-3") 'resizeHori)
(global-set-key (kbd "M-1") 'resizeFull)

;;; for ease of use from my French laptop
(global-set-key (kbd "M-é") 'resizeVert)
(global-set-key (kbd "M-\"") 'resizeHori)
(global-set-key (kbd "M-&") 'resizeFull)

;;; ** Revert buffer

;;; http://stackoverflow.com/questions/17050810/how-can-i-tell-emacs-my-git-branch-has-changed
(global-set-key [f5] (lambda () (interactive) (revert-buffer nil t)))

;;; ** Switch to other window

;;; http://stackoverflow.com/questions/91071/emacs-switch-to-previous-window
(defun prev-window ()
  (interactive) (other-window -1))

;;; http://emacs.stackexchange.com/questions/3458/how-to-switch-between-windows-quickly
(global-set-key (kbd "C-<right>") 'other-window)
(global-set-key (kbd "C-<left>") 'prev-window)

;;; https://www.emacswiki.org/emacs/SwitchingBuffers
(global-set-key (kbd "M-s")  'mode-line-other-buffer)

;;; ** Buffer menu

;;; http://www.emacswiki.org/emacs/BufferMenu
(global-set-key (kbd "C-x C-b") 'buffer-menu) ; or 'buffer-menu-other-window

;;; ** Switching buffers

;;; https://www.emacswiki.org/emacs/SwitchingBuffers
(global-set-key (kbd "C-<f6>") 'previous-buffer)
(global-set-key (kbd "C-<f7>") 'next-buffer)

;;; * Copy and paste, system-wide

;;; http://stackoverflow.com/questions/64360/how-to-copy-text-from-emacs-to-another-application-on-linux/19625063#19625063
(defun copy-to-clipboard ()
  (interactive)
  (if (display-graphic-p)
      (progn
	(message "Yanked region to x-clipboard!")
	(call-interactively 'clipboard-kill-ring-save)
	)
    (if (region-active-p)
	(progn
	  (shell-command-on-region (region-beginning) (region-end) "xsel -i -b")
	  (message "Yanked region to clipboard!")
	  (deactivate-mark))
      (message "No region active; can't yank to clipboard!")))
  )

(defun paste-from-clipboard ()
  (interactive)
  (if (display-graphic-p)
      (progn
	(clipboard-yank)
	(message "graphics active")
	)
    (insert (shell-command-to-string "xsel -o -b"))
    )
  )

(global-set-key [f8] 'copy-to-clipboard)
(global-set-key [f9] 'paste-from-clipboard)

;;; * Yank forward (M-S-y)

;;; https://www.emacswiki.org/emacs/KillingAndYanking
(defun yank-pop-forwards (arg)
  (interactive "p")
  (yank-pop (- arg)))

(global-set-key "\M-Y" 'yank-pop-forwards) ; M-Y (Meta-Shift-Y)

;;; * Goto random line (goto-random-line)

;;; https://www.emacswiki.org/emacs/goto-random-line.el
;;; Jump to a random line, sburke@cpan.org
(defun goto-random-line ()
  "Go to a random line in this buffer." ; good for electrobibliomancy.
  (interactive)
  (goto-line (1+ (random (buffer-line-count)))))
(defun buffer-line-count ()
  "Return the number of lines in this buffer."
    (count-lines (point-min) (point-max)))

;;; * Ibuffer configuration

;;; ** Hide polymode extra buffers
;;; Hide polymode duplicated buffers
(setq ibuffer-never-show-predicates nil)
;; (add-to-list 'ibuffer-never-show-predicates "\\[R\\]")
;; (add-to-list 'ibuffer-never-show-predicates "\\[yaml\\]")
;; (add-to-list 'ibuffer-never-show-predicates "\\[latex\\]")

;;; * Other key bindings

;;; ** Magit
(global-set-key (kbd "C-x g") 'magit-status)

;;; * Load other configuration files

;;; ** Load ESS-related configuration
(load "~/.emacs.d/dot-emacs.ess")

;;; * Customize

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ess-R-font-lock-keywords
   (quote
    ((ess-R-fl-keyword:modifiers . t)
     (ess-R-fl-keyword:fun-defs . t)
     (ess-R-fl-keyword:keywords . t)
     (ess-R-fl-keyword:assign-ops . t)
     (ess-R-fl-keyword:constants . t)
     (ess-fl-keyword:fun-calls . t)
     (ess-fl-keyword:numbers)
     (ess-fl-keyword:operators . t)
     (ess-fl-keyword:delimiters)
     (ess-fl-keyword:=)
     (ess-R-fl-keyword:F&T)
     (ess-R-fl-keyword:%op% . t))))
 '(history-length t)
 '(org-use-speed-commands t)
 '(package-selected-packages (quote (magit markdown-mode polymode w3m yaml-mode jabber))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(markdown-italic-face ((t nil)))
 '(org-hide ((t (:foreground "#4e4e4e")))))
(put 'downcase-region 'disabled nil)

;;; * Python

(setq python-shell-interpreter "ipython3"
      python-shell-interpreter-args "--simple-prompt -i")

;;; * Shell script

;;; https://emacs.stackexchange.com/questions/37887/send-region-to-shell-in-another-buffer
(defun tws-region-to-process (arg beg end)
  "Send the current region to a process buffer"
  (interactive "P\nr")
  (when (or arg
            (not (boundp 'tws-process-target))
            (not (process-live-p (get-buffer-process tws-process-target))))
    (let (procs buf)
      (setq procs (remove nil (seq-map
                               (lambda (el)
                                 (when (setq buf (process-buffer el))
                                   (buffer-name buf)))
                               (process-list))))
      (if (not procs) (error "No process buffers currently open.")
        (setq-local tws-process-target (completing-read "Process: " procs)))))
  (process-send-region tws-process-target beg end))

(defun bindkey-sh-region-send-to-process ()
  (local-set-key (kbd "C-c C-p") 'tws-region-to-process)
  )

(add-hook 'sh-mode-hook 'bindkey-sh-region-send-to-process)

;;; * Manual mode associations

;;; Associate stan files with C++ mode
(add-to-list 'auto-mode-alist '("\\.stan\\'" . c++-mode))

;;; * Color file completion list

;;; Modified from https://github.com/vjohansen/emacs-config/blob/master/site-lisp/color-file-completion.el
(defface completion-setup-directory-face '((t (:foreground "#5fafff")))
  "Face to use for directories"
  :group 'color-file-completion)

(defface completion-setup-R-script-face '((t (:foreground "yellow")))
  "Face to use for R scripts"
  :group 'color-file-completion)

(defface completion-setup-Rmd-face '((t (:foreground "#00cb5f")))
  "Face to use for R markdown documents"
  :group 'color-file-completion)

(defface completion-setup-hidden-face '((t (:foreground "#727272")))
  "Face to use for hidden files"
  :group 'color-file-completion)

(defcustom color-file-completion-always t
  "If true, always turn on regexps in completion buffers"
  :group 'color-file-completion
  :type 'boolean)

(defun completion-setup-directory-face()
  "Highlight directories when completing a filename"
  (interactive)

  (when (or color-file-completion-always
            (eq minibuffer-completion-table 'read-file-name-internal))

    (let ((font-lock-verbose nil))
      (font-lock-mode 1)
      (font-lock-add-keywords nil '(("[^ \n]+/" 0 'completion-setup-directory-face keep)))
      (font-lock-fontify-buffer)
      )

    (let ((font-lock-verbose nil))
      (font-lock-mode 1)
      (font-lock-add-keywords nil '(("[^ \n]+[.]R[ \n]" 0 'completion-setup-R-script-face keep)))
      (font-lock-add-keywords nil '(("[^ \n]+[.]R$" 0 'completion-setup-R-script-face keep)))
      (font-lock-fontify-buffer)
      )

    (let ((font-lock-verbose nil))
      (font-lock-mode 1)
      (font-lock-add-keywords nil '(("[^ \n]+[.]Rmd[ \n]" 0 'completion-setup-Rmd-face keep)))
      (font-lock-add-keywords nil '(("[^ \n]+[.]Rmd$" 0 'completion-setup-Rmd-face keep)))
      (font-lock-fontify-buffer)
      )

    (let ((font-lock-verbose nil))
      (font-lock-mode 1)
      (font-lock-add-keywords nil '(("\t[.]+[^./ \n]+" 0 'completion-setup-hidden-face keep)))
      (font-lock-add-keywords nil '(("^[.]+[^./ \n]+" 0 'completion-setup-hidden-face keep)))
      (font-lock-fontify-buffer)
      )

    )
  )

(add-hook 'completion-list-mode-hook 'completion-setup-directory-face)

;;; * C mode

;;; https://stackoverflow.com/questions/14715181/emacs-function-call-highlight/14716203
(font-lock-add-keywords
 'c-mode
 '(("\\<\\(\\sw+\\) ?(" 1 'font-lock-function-name-face)))

