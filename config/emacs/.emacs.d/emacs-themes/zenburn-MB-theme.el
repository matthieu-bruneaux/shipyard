;;; zenburn-MB-theme.el --- A low contrast color theme for Emacs.
;;; with slight modifications from Matthieu Bruneaux

;; Copyright (C) 2011-2014 Bozhidar Batsov

;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; URL: http://github.com/bbatsov/zenburn-MB-emacs
;; Version: 20150214.131
;; X-Original-Version: 2.3-cvs

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; A port of the popular Vim theme Zenburn-MB for Emacs 24, built on top
;; of the new built-in theme support in Emacs 24.

;;; Credits:

;; Jani Nurminen created the original theme for vim on such this port
;; is based.

;;; Code:

(deftheme zenburn-MB "The Zenburn-MB color theme, customized by MB")

;;; Color Palette

(defvar zenburn-MB-colors-alist
  '(("zenburn-MB-fg+1"     . "#FFFFEF")
    ("zenburn-MB-fg"       . "#DCDCCC")
    ("zenburn-MB-fg-1"     . "#656555")
    ("zenburn-MB-bg-2"     . "#000000")
    ("zenburn-MB-bg-1"     . "#2B2B2B")
    ("zenburn-MB-bg-05"    . "#383838")
    ("zenburn-MB-bg"       . "#3F3F3F")
    ("zenburn-MB-bg+05"    . "#494949")
    ("zenburn-MB-bg+1"     . "#4F4F4F")
    ("zenburn-MB-bg+2"     . "#5F5F5F")
    ("zenburn-MB-bg+3"     . "#6F6F6F")
    ("zenburn-MB-red+1"    . "#DCA3A3")
    ("zenburn-MB-red"      . "#CC9393")
    ("zenburn-MB-red-1"    . "#BC8383")
    ("zenburn-MB-red-2"    . "#AC7373")
    ("zenburn-MB-red-3"    . "#9C6363")
    ("zenburn-MB-red-4"    . "#8C5353")
    ("zenburn-MB-orange"   . "#DFAF8F")
    ("zenburn-MB-yellow"   . "#F0DFAF")
    ("zenburn-MB-yellow-1" . "#E0CF9F")
    ("zenburn-MB-yellow-2" . "#D0BF8F")
    ("zenburn-MB-green-1"  . "#5F7F5F")
    ("zenburn-MB-green"    . "#7F9F7F")
    ("zenburn-MB-green+1"  . "#8FB28F")
    ("zenburn-MB-green+2"  . "#9FC59F")
    ("zenburn-MB-green+3"  . "#AFD8AF")
    ("zenburn-MB-green+4"  . "#BFEBBF")
    ("zenburn-MB-cyan"     . "#93E0E3")
    ("zenburn-MB-blue+1"   . "#94BFF3")
    ("zenburn-MB-blue"     . "#8CD0D3")
    ("zenburn-MB-blue-1"   . "#7CB8BB")
    ("zenburn-MB-blue-2"   . "#6CA0A3")
    ("zenburn-MB-blue-3"   . "#5C888B")
    ("zenburn-MB-blue-4"   . "#4C7073")
    ("zenburn-MB-blue-5"   . "#366060")
    ("zenburn-MB-blue-5"   . "#366060")
    ("zenburn-MB-magenta"  . "#DC8CC3"))
  "List of Zenburn-MB colors.
Each element has the form (NAME . HEX).

`+N' suffixes indicate a color is lighter.
`-N' suffixes indicate a color is darker.")

(defmacro zenburn-MB-with-color-variables (&rest body)
  "`let' bind all colors defined in `zenburn-MB-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
         ,@(mapcar (lambda (cons)
                     (list (intern (car cons)) (cdr cons)))
                   zenburn-MB-colors-alist))
     ,@body))

;;; Theme Faces
(zenburn-MB-with-color-variables
  (custom-theme-set-faces
   'zenburn-MB
;;;; Built-in
;;;;; basic coloring
   '(button ((t (:underline t))))
   `(link ((t (:foreground ,zenburn-MB-yellow :underline t :weight bold))))
   `(link-visited ((t (:foreground ,zenburn-MB-yellow-2 :underline t :weight normal))))
   `(default ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg))))
   `(cursor ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-fg+1))))
   `(escape-glyph ((t (:foreground ,zenburn-MB-yellow :bold t))))
   `(fringe ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg+1))))
   `(header-line ((t (:foreground ,zenburn-MB-yellow
                                  :background ,zenburn-MB-bg-1
                                  :box (:line-width -1 :style released-button)))))
   `(highlight ((t (:background ,zenburn-MB-bg-05))))
   `(success ((t (:foreground ,zenburn-MB-green :weight bold))))
   `(warning ((t (:foreground ,zenburn-MB-orange :weight bold))))
;;;;; compilation
   `(compilation-column-face ((t (:foreground ,zenburn-MB-yellow))))
   `(compilation-enter-directory-face ((t (:foreground ,zenburn-MB-green))))
   `(compilation-error-face ((t (:foreground ,zenburn-MB-red-1 :weight bold :underline t))))
   `(compilation-face ((t (:foreground ,zenburn-MB-fg))))
   `(compilation-info-face ((t (:foreground ,zenburn-MB-blue))))
   `(compilation-info ((t (:foreground ,zenburn-MB-green+4 :underline t))))
   `(compilation-leave-directory-face ((t (:foreground ,zenburn-MB-green))))
   `(compilation-line-face ((t (:foreground ,zenburn-MB-yellow))))
   `(compilation-line-number ((t (:foreground ,zenburn-MB-yellow))))
   `(compilation-message-face ((t (:foreground ,zenburn-MB-blue))))
   `(compilation-warning-face ((t (:foreground ,zenburn-MB-orange :weight bold :underline t))))
   `(compilation-mode-line-exit ((t (:foreground ,zenburn-MB-green+2 :weight bold))))
   `(compilation-mode-line-fail ((t (:foreground ,zenburn-MB-red :weight bold))))
   `(compilation-mode-line-run ((t (:foreground ,zenburn-MB-yellow :weight bold))))
;;;;; grep
   `(grep-context-face ((t (:foreground ,zenburn-MB-fg))))
   `(grep-error-face ((t (:foreground ,zenburn-MB-red-1 :weight bold :underline t))))
   `(grep-hit-face ((t (:foreground ,zenburn-MB-blue))))
   `(grep-match-face ((t (:foreground ,zenburn-MB-orange :weight bold))))
   `(match ((t (:background ,zenburn-MB-bg-1 :foreground ,zenburn-MB-orange :weight bold))))
;;;;; isearch
   `(isearch ((t (:foreground ,zenburn-MB-yellow-2 :weight bold :background ,zenburn-MB-bg+2))))
   `(isearch-fail ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-red-4))))
   `(lazy-highlight ((t (:foreground ,zenburn-MB-yellow-2 :weight bold :background ,zenburn-MB-bg-05))))

   `(menu ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg))))
   `(minibuffer-prompt ((t (:foreground ,zenburn-MB-yellow))))
   `(mode-line
     ((,class (:foreground ,zenburn-MB-green+1
                           :background ,zenburn-MB-bg-1
                           :box (:line-width -1 :style released-button)))
      (t :inverse-video t)))
   `(mode-line-buffer-id ((t (:foreground ,zenburn-MB-yellow :weight normal))))
   `(mode-line-inactive
     ((t (:foreground ,zenburn-MB-bg
                      :background ,zenburn-MB-bg-1
                      :box (:line-width -1 :style released-button)))))
   `(region ((,class (:background ,zenburn-MB-blue-5))
             (t :inverse-video t)))
   `(secondary-selection ((t (:background ,zenburn-MB-bg+2))))
   `(trailing-whitespace ((t (:background ,zenburn-MB-red))))
   `(vertical-border ((t (:foreground ,zenburn-MB-fg))))
;;;;; font lock
   `(font-lock-builtin-face ((t (:foreground ,zenburn-MB-fg :weight bold))))
   `(font-lock-comment-face ((t (:foreground ,zenburn-MB-green))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,zenburn-MB-green-1))))
   `(font-lock-constant-face ((t (:foreground ,zenburn-MB-green+4))))
   `(font-lock-doc-face ((t (:foreground ,zenburn-MB-green+2))))
   `(font-lock-function-name-face ((t (:foreground ,zenburn-MB-cyan))))
   `(font-lock-keyword-face ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(font-lock-negation-char-face ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(font-lock-preprocessor-face ((t (:foreground ,zenburn-MB-blue+1))))
   `(font-lock-regexp-grouping-construct ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(font-lock-regexp-grouping-backslash ((t (:foreground ,zenburn-MB-green :weight bold))))
   `(font-lock-string-face ((t (:foreground ,zenburn-MB-red))))
   `(font-lock-type-face ((t (:foreground ,zenburn-MB-blue-1))))
   `(font-lock-variable-name-face ((t (:foreground ,zenburn-MB-orange))))
   `(font-lock-warning-face ((t (:foreground ,zenburn-MB-yellow-2 :weight bold))))

   `(c-annotation-face ((t (:inherit font-lock-constant-face))))
;;;;; newsticker
   `(newsticker-date-face ((t (:foreground ,zenburn-MB-fg))))
   `(newsticker-default-face ((t (:foreground ,zenburn-MB-fg))))
   `(newsticker-enclosure-face ((t (:foreground ,zenburn-MB-green+3))))
   `(newsticker-extra-face ((t (:foreground ,zenburn-MB-bg+2 :height 0.8))))
   `(newsticker-feed-face ((t (:foreground ,zenburn-MB-fg))))
   `(newsticker-immortal-item-face ((t (:foreground ,zenburn-MB-green))))
   `(newsticker-new-item-face ((t (:foreground ,zenburn-MB-blue))))
   `(newsticker-obsolete-item-face ((t (:foreground ,zenburn-MB-red))))
   `(newsticker-old-item-face ((t (:foreground ,zenburn-MB-bg+3))))
   `(newsticker-statistics-face ((t (:foreground ,zenburn-MB-fg))))
   `(newsticker-treeview-face ((t (:foreground ,zenburn-MB-fg))))
   `(newsticker-treeview-immortal-face ((t (:foreground ,zenburn-MB-green))))
   `(newsticker-treeview-listwindow-face ((t (:foreground ,zenburn-MB-fg))))
   `(newsticker-treeview-new-face ((t (:foreground ,zenburn-MB-blue :weight bold))))
   `(newsticker-treeview-obsolete-face ((t (:foreground ,zenburn-MB-red))))
   `(newsticker-treeview-old-face ((t (:foreground ,zenburn-MB-bg+3))))
   `(newsticker-treeview-selection-face ((t (:background ,zenburn-MB-bg-1 :foreground ,zenburn-MB-yellow))))
;;;; Third-party
;;;;; ace-jump
   `(ace-jump-face-background
     ((t (:foreground ,zenburn-MB-fg-1 :background ,zenburn-MB-bg :inverse-video nil))))
   `(ace-jump-face-foreground
     ((t (:foreground ,zenburn-MB-green+2 :background ,zenburn-MB-bg :inverse-video nil))))
;;;;; android mode
   `(android-mode-debug-face ((t (:foreground ,zenburn-MB-green+1))))
   `(android-mode-error-face ((t (:foreground ,zenburn-MB-orange :weight bold))))
   `(android-mode-info-face ((t (:foreground ,zenburn-MB-fg))))
   `(android-mode-verbose-face ((t (:foreground ,zenburn-MB-green))))
   `(android-mode-warning-face ((t (:foreground ,zenburn-MB-yellow))))
;;;;; anzu
   `(anzu-mode-line ((t (:foreground ,zenburn-MB-cyan :weight bold))))
;;;;; auctex
   `(font-latex-bold-face ((t (:inherit bold))))
   `(font-latex-warning-face ((t (:foreground nil :inherit font-lock-warning-face))))
   `(font-latex-sectioning-5-face ((t (:foreground ,zenburn-MB-red :weight bold ))))
   `(font-latex-sedate-face ((t (:foreground ,zenburn-MB-yellow))))
   `(font-latex-italic-face ((t (:foreground ,zenburn-MB-cyan :slant italic))))
   `(font-latex-string-face ((t (:inherit ,font-lock-string-face))))
   `(font-latex-math-face ((t (:foreground ,zenburn-MB-orange))))
;;;;; auto-complete
   `(ac-candidate-face ((t (:background ,zenburn-MB-bg+3 :foreground ,zenburn-MB-bg-2))))
   `(ac-selection-face ((t (:background ,zenburn-MB-blue-4 :foreground ,zenburn-MB-fg))))
   `(popup-tip-face ((t (:background ,zenburn-MB-yellow-2 :foreground ,zenburn-MB-bg-2))))
   `(popup-scroll-bar-foreground-face ((t (:background ,zenburn-MB-blue-5))))
   `(popup-scroll-bar-background-face ((t (:background ,zenburn-MB-bg-1))))
   `(popup-isearch-match ((t (:background ,zenburn-MB-bg :foreground ,zenburn-MB-fg))))
;;;;; company-mode
   `(company-tooltip ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg+1))))
   `(company-tooltip-selection ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg-1))))
   `(company-tooltip-mouse ((t (:background ,zenburn-MB-bg-1))))
   `(company-tooltip-common ((t (:foreground ,zenburn-MB-green+2))))
   `(company-tooltip-common-selection ((t (:foreground ,zenburn-MB-green+2))))
   `(company-scrollbar-fg ((t (:background ,zenburn-MB-bg-1))))
   `(company-scrollbar-bg ((t (:background ,zenburn-MB-bg+2))))
   `(company-preview ((t (:background ,zenburn-MB-green+2))))
   `(company-preview-common ((t (:foreground ,zenburn-MB-green+2 :background ,zenburn-MB-bg-1))))
;;;;; bm
   `(bm-face ((t (:background ,zenburn-MB-yellow-1 :foreground ,zenburn-MB-bg))))
   `(bm-fringe-face ((t (:background ,zenburn-MB-yellow-1 :foreground ,zenburn-MB-bg))))
   `(bm-fringe-persistent-face ((t (:background ,zenburn-MB-green-1 :foreground ,zenburn-MB-bg))))
   `(bm-persistent-face ((t (:background ,zenburn-MB-green-1 :foreground ,zenburn-MB-bg))))
;;;;; clojure-test-mode
   `(clojure-test-failure-face ((t (:foreground ,zenburn-MB-orange :weight bold :underline t))))
   `(clojure-test-error-face ((t (:foreground ,zenburn-MB-red :weight bold :underline t))))
   `(clojure-test-success-face ((t (:foreground ,zenburn-MB-green+1 :weight bold :underline t))))
;;;;; coq
   `(coq-solve-tactics-face ((t (:foreground nil :inherit font-lock-constant-face))))
;;;;; ctable
   `(ctbl:face-cell-select ((t (:background ,zenburn-MB-blue :foreground ,zenburn-MB-bg))))
   `(ctbl:face-continue-bar ((t (:background ,zenburn-MB-bg-05 :foreground ,zenburn-MB-bg))))
   `(ctbl:face-row-select ((t (:background ,zenburn-MB-cyan :foreground ,zenburn-MB-bg))))
;;;;; diff
   `(diff-added ((,class (:foreground ,zenburn-MB-green+4 :background nil))
                 (t (:foreground ,zenburn-MB-green-1 :background nil))))
   `(diff-changed ((t (:foreground ,zenburn-MB-yellow))))
   `(diff-removed ((,class (:foreground ,zenburn-MB-red :background nil))
                   (t (:foreground ,zenburn-MB-red-3 :background nil))))
   `(diff-refine-added ((t (:inherit diff-added :weight bold))))
   `(diff-refine-change ((t (:inherit diff-changed :weight bold))))
   `(diff-refine-removed ((t (:inherit diff-removed :weight bold))))
   `(diff-header ((,class (:background ,zenburn-MB-bg+2))
                  (t (:background ,zenburn-MB-fg :foreground ,zenburn-MB-bg))))
   `(diff-file-header
     ((,class (:background ,zenburn-MB-bg+2 :foreground ,zenburn-MB-fg :bold t))
      (t (:background ,zenburn-MB-fg :foreground ,zenburn-MB-bg :bold t))))
;;;;; diff-hl
   `(diff-hl-change ((,class (:foreground ,zenburn-MB-blue-2 :background ,zenburn-MB-bg-05))))
   `(diff-hl-delete ((,class (:foreground ,zenburn-MB-red+1 :background ,zenburn-MB-bg-05))))
   `(diff-hl-insert ((,class (:foreground ,zenburn-MB-green+1 :background ,zenburn-MB-bg-05))))
   `(diff-hl-unknown ((,class (:foreground ,zenburn-MB-yellow :background ,zenburn-MB-bg-05))))
;;;;; dim-autoload
   `(dim-autoload-cookie-line ((t :foreground ,zenburn-MB-bg+1)))
;;;;; dired+
   `(diredp-display-msg ((t (:foreground ,zenburn-MB-blue))))
   `(diredp-compressed-file-suffix ((t (:foreground ,zenburn-MB-orange))))
   `(diredp-date-time ((t (:foreground ,zenburn-MB-magenta))))
   `(diredp-deletion ((t (:foreground ,zenburn-MB-yellow))))
   `(diredp-deletion-file-name ((t (:foreground ,zenburn-MB-red))))
   `(diredp-dir-heading ((t (:foreground ,zenburn-MB-blue :background ,zenburn-MB-bg-1))))
   `(diredp-dir-priv ((t (:foreground ,zenburn-MB-cyan))))
   `(diredp-exec-priv ((t (:foreground ,zenburn-MB-red))))
   `(diredp-executable-tag ((t (:foreground ,zenburn-MB-green+1))))
   `(diredp-file-name ((t (:foreground ,zenburn-MB-blue))))
   `(diredp-file-suffix ((t (:foreground ,zenburn-MB-green))))
   `(diredp-flag-mark ((t (:foreground ,zenburn-MB-yellow))))
   `(diredp-flag-mark-line ((t (:foreground ,zenburn-MB-orange))))
   `(diredp-ignored-file-name ((t (:foreground ,zenburn-MB-red))))
   `(diredp-link-priv ((t (:foreground ,zenburn-MB-yellow))))
   `(diredp-mode-line-flagged ((t (:foreground ,zenburn-MB-yellow))))
   `(diredp-mode-line-marked ((t (:foreground ,zenburn-MB-orange))))
   `(diredp-no-priv ((t (:foreground ,zenburn-MB-fg))))
   `(diredp-number ((t (:foreground ,zenburn-MB-green+1))))
   `(diredp-other-priv ((t (:foreground ,zenburn-MB-yellow-1))))
   `(diredp-rare-priv ((t (:foreground ,zenburn-MB-red-1))))
   `(diredp-read-priv ((t (:foreground ,zenburn-MB-green-1))))
   `(diredp-symlink ((t (:foreground ,zenburn-MB-yellow))))
   `(diredp-write-priv ((t (:foreground ,zenburn-MB-magenta))))
;;;;; ediff
   `(ediff-current-diff-A ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-red-4))))
   `(ediff-current-diff-Ancestor ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-red-4))))
   `(ediff-current-diff-B ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-green-1))))
   `(ediff-current-diff-C ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-blue-5))))
   `(ediff-even-diff-A ((t (:background ,zenburn-MB-bg+1))))
   `(ediff-even-diff-Ancestor ((t (:background ,zenburn-MB-bg+1))))
   `(ediff-even-diff-B ((t (:background ,zenburn-MB-bg+1))))
   `(ediff-even-diff-C ((t (:background ,zenburn-MB-bg+1))))
   `(ediff-fine-diff-A ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-red-2 :weight bold))))
   `(ediff-fine-diff-Ancestor ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-red-2 weight bold))))
   `(ediff-fine-diff-B ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-green :weight bold))))
   `(ediff-fine-diff-C ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-blue-3 :weight bold ))))
   `(ediff-odd-diff-A ((t (:background ,zenburn-MB-bg+2))))
   `(ediff-odd-diff-Ancestor ((t (:background ,zenburn-MB-bg+2))))
   `(ediff-odd-diff-B ((t (:background ,zenburn-MB-bg+2))))
   `(ediff-odd-diff-C ((t (:background ,zenburn-MB-bg+2))))
;;;;; egg
   `(egg-text-base ((t (:foreground ,zenburn-MB-fg))))
   `(egg-help-header-1 ((t (:foreground ,zenburn-MB-yellow))))
   `(egg-help-header-2 ((t (:foreground ,zenburn-MB-green+3))))
   `(egg-branch ((t (:foreground ,zenburn-MB-yellow))))
   `(egg-branch-mono ((t (:foreground ,zenburn-MB-yellow))))
   `(egg-term ((t (:foreground ,zenburn-MB-yellow))))
   `(egg-diff-add ((t (:foreground ,zenburn-MB-green+4))))
   `(egg-diff-del ((t (:foreground ,zenburn-MB-red+1))))
   `(egg-diff-file-header ((t (:foreground ,zenburn-MB-yellow-2))))
   `(egg-section-title ((t (:foreground ,zenburn-MB-yellow))))
   `(egg-stash-mono ((t (:foreground ,zenburn-MB-green+4))))
;;;;; elfeed
   `(elfeed-search-date-face ((t (:foreground ,zenburn-MB-yellow-1 :underline t
                                              :weight bold))))
   `(elfeed-search-tag-face ((t (:foreground ,zenburn-MB-green))))
   `(elfeed-search-feed-face ((t (:foreground ,zenburn-MB-cyan))))
;;;;; emacs-w3m
   `(w3m-anchor ((t (:foreground ,zenburn-MB-yellow :underline t
                                 :weight bold))))
   `(w3m-arrived-anchor ((t (:foreground ,zenburn-MB-yellow-2
                                         :underline t :weight normal))))
   `(w3m-form ((t (:foreground ,zenburn-MB-red-1 :underline t))))
   `(w3m-header-line-location-title ((t (:foreground ,zenburn-MB-yellow
                                                     :underline t :weight bold))))
   '(w3m-history-current-url ((t (:inherit match))))
   `(w3m-lnum ((t (:foreground ,zenburn-MB-green+2 :background ,zenburn-MB-bg))))
   `(w3m-lnum-match ((t (:background ,zenburn-MB-bg-1
                                     :foreground ,zenburn-MB-orange
                                     :weight bold))))
   `(w3m-lnum-minibuffer-prompt ((t (:foreground ,zenburn-MB-yellow))))
;;;;; erc
   `(erc-action-face ((t (:inherit erc-default-face))))
   `(erc-bold-face ((t (:weight bold))))
   `(erc-current-nick-face ((t (:foreground ,zenburn-MB-blue :weight bold))))
   `(erc-dangerous-host-face ((t (:inherit font-lock-warning-face))))
   `(erc-default-face ((t (:foreground ,zenburn-MB-fg))))
   `(erc-direct-msg-face ((t (:inherit erc-default))))
   `(erc-error-face ((t (:inherit font-lock-warning-face))))
   `(erc-fool-face ((t (:inherit erc-default))))
   `(erc-highlight-face ((t (:inherit hover-highlight))))
   `(erc-input-face ((t (:foreground ,zenburn-MB-yellow))))
   `(erc-keyword-face ((t (:foreground ,zenburn-MB-blue :weight bold))))
   `(erc-nick-default-face ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(erc-my-nick-face ((t (:foreground ,zenburn-MB-red :weight bold))))
   `(erc-nick-msg-face ((t (:inherit erc-default))))
   `(erc-notice-face ((t (:foreground ,zenburn-MB-green))))
   `(erc-pal-face ((t (:foreground ,zenburn-MB-orange :weight bold))))
   `(erc-prompt-face ((t (:foreground ,zenburn-MB-orange :background ,zenburn-MB-bg :weight bold))))
   `(erc-timestamp-face ((t (:foreground ,zenburn-MB-green+4))))
   `(erc-underline-face ((t (:underline t))))
;;;;; ert
   `(ert-test-result-expected ((t (:foreground ,zenburn-MB-green+4 :background ,zenburn-MB-bg))))
   `(ert-test-result-unexpected ((t (:foreground ,zenburn-MB-red :background ,zenburn-MB-bg))))
;;;;; eshell
   `(eshell-prompt ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(eshell-ls-archive ((t (:foreground ,zenburn-MB-red-1 :weight bold))))
   `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-directory ((t (:foreground ,zenburn-MB-blue+1 :weight bold))))
   `(eshell-ls-executable ((t (:foreground ,zenburn-MB-red+1 :weight bold))))
   `(eshell-ls-unreadable ((t (:foreground ,zenburn-MB-fg))))
   `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
   `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
   `(eshell-ls-special ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(eshell-ls-symlink ((t (:foreground ,zenburn-MB-cyan :weight bold))))
;;;;; flx
   `(flx-highlight-face ((t (:foreground ,zenburn-MB-green+2 :weight bold))))
;;;;; flycheck
   `(flycheck-error
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-red-1) :inherit unspecified))
      (t (:foreground ,zenburn-MB-red-1 :weight bold :underline t))))
   `(flycheck-warning
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-yellow) :inherit unspecified))
      (t (:foreground ,zenburn-MB-yellow :weight bold :underline t))))
   `(flycheck-info
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-cyan) :inherit unspecified))
      (t (:foreground ,zenburn-MB-cyan :weight bold :underline t))))
   `(flycheck-fringe-error ((t (:foreground ,zenburn-MB-red-1 :weight bold))))
   `(flycheck-fringe-warning ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(flycheck-fringe-info ((t (:foreground ,zenburn-MB-cyan :weight bold))))
;;;;; flymake
   `(flymake-errline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-red)
                   :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,zenburn-MB-red-1 :weight bold :underline t))))
   `(flymake-warnline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-orange)
                   :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,zenburn-MB-orange :weight bold :underline t))))
   `(flymake-infoline
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-green)
                   :inherit unspecified :foreground unspecified :background unspecified))
      (t (:foreground ,zenburn-MB-green-1 :weight bold :underline t))))
;;;;; flyspell
   `(flyspell-duplicate
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-orange) :inherit unspecified))
      (t (:foreground ,zenburn-MB-orange :weight bold :underline t))))
   `(flyspell-incorrect
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-red) :inherit unspecified))
      (t (:foreground ,zenburn-MB-red-1 :weight bold :underline t))))
;;;;; full-ack
   `(ack-separator ((t (:foreground ,zenburn-MB-fg))))
   `(ack-file ((t (:foreground ,zenburn-MB-blue))))
   `(ack-line ((t (:foreground ,zenburn-MB-yellow))))
   `(ack-match ((t (:foreground ,zenburn-MB-orange :background ,zenburn-MB-bg-1 :weight bold))))
;;;;; git-gutter
   `(git-gutter:added ((t (:foreground ,zenburn-MB-green :weight bold :inverse-video t))))
   `(git-gutter:deleted ((t (:foreground ,zenburn-MB-red :weight bold :inverse-video t))))
   `(git-gutter:modified ((t (:foreground ,zenburn-MB-magenta :weight bold :inverse-video t))))
   `(git-gutter:unchanged ((t (:foreground ,zenburn-MB-fg :weight bold :inverse-video t))))
;;;;; git-gutter-fr
   `(git-gutter-fr:added ((t (:foreground ,zenburn-MB-green  :weight bold))))
   `(git-gutter-fr:deleted ((t (:foreground ,zenburn-MB-red :weight bold))))
   `(git-gutter-fr:modified ((t (:foreground ,zenburn-MB-magenta :weight bold))))
;;;;; git-rebase-mode
   `(git-rebase-hash ((t (:foreground, zenburn-MB-orange))))
;;;;; gnus
   `(gnus-group-mail-1 ((t (:bold t :inherit gnus-group-mail-1-empty))))
   `(gnus-group-mail-1-empty ((t (:inherit gnus-group-news-1-empty))))
   `(gnus-group-mail-2 ((t (:bold t :inherit gnus-group-mail-2-empty))))
   `(gnus-group-mail-2-empty ((t (:inherit gnus-group-news-2-empty))))
   `(gnus-group-mail-3 ((t (:bold t :inherit gnus-group-mail-3-empty))))
   `(gnus-group-mail-3-empty ((t (:inherit gnus-group-news-3-empty))))
   `(gnus-group-mail-4 ((t (:bold t :inherit gnus-group-mail-4-empty))))
   `(gnus-group-mail-4-empty ((t (:inherit gnus-group-news-4-empty))))
   `(gnus-group-mail-5 ((t (:bold t :inherit gnus-group-mail-5-empty))))
   `(gnus-group-mail-5-empty ((t (:inherit gnus-group-news-5-empty))))
   `(gnus-group-mail-6 ((t (:bold t :inherit gnus-group-mail-6-empty))))
   `(gnus-group-mail-6-empty ((t (:inherit gnus-group-news-6-empty))))
   `(gnus-group-mail-low ((t (:bold t :inherit gnus-group-mail-low-empty))))
   `(gnus-group-mail-low-empty ((t (:inherit gnus-group-news-low-empty))))
   `(gnus-group-news-1 ((t (:bold t :inherit gnus-group-news-1-empty))))
   `(gnus-group-news-2 ((t (:bold t :inherit gnus-group-news-2-empty))))
   `(gnus-group-news-3 ((t (:bold t :inherit gnus-group-news-3-empty))))
   `(gnus-group-news-4 ((t (:bold t :inherit gnus-group-news-4-empty))))
   `(gnus-group-news-5 ((t (:bold t :inherit gnus-group-news-5-empty))))
   `(gnus-group-news-6 ((t (:bold t :inherit gnus-group-news-6-empty))))
   `(gnus-group-news-low ((t (:bold t :inherit gnus-group-news-low-empty))))
   `(gnus-header-content ((t (:inherit message-header-other))))
   `(gnus-header-from ((t (:inherit message-header-from))))
   `(gnus-header-name ((t (:inherit message-header-name))))
   `(gnus-header-newsgroups ((t (:inherit message-header-other))))
   `(gnus-header-subject ((t (:inherit message-header-subject))))
   `(gnus-summary-cancelled ((t (:foreground ,zenburn-MB-orange))))
   `(gnus-summary-high-ancient ((t (:foreground ,zenburn-MB-blue))))
   `(gnus-summary-high-read ((t (:foreground ,zenburn-MB-green :weight bold))))
   `(gnus-summary-high-ticked ((t (:foreground ,zenburn-MB-orange :weight bold))))
   `(gnus-summary-high-unread ((t (:foreground ,zenburn-MB-fg :weight bold))))
   `(gnus-summary-low-ancient ((t (:foreground ,zenburn-MB-blue))))
   `(gnus-summary-low-read ((t (:foreground ,zenburn-MB-green))))
   `(gnus-summary-low-ticked ((t (:foreground ,zenburn-MB-orange :weight bold))))
   `(gnus-summary-low-unread ((t (:foreground ,zenburn-MB-fg))))
   `(gnus-summary-normal-ancient ((t (:foreground ,zenburn-MB-blue))))
   `(gnus-summary-normal-read ((t (:foreground ,zenburn-MB-green))))
   `(gnus-summary-normal-ticked ((t (:foreground ,zenburn-MB-orange :weight bold))))
   `(gnus-summary-normal-unread ((t (:foreground ,zenburn-MB-fg))))
   `(gnus-summary-selected ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(gnus-cite-1 ((t (:foreground ,zenburn-MB-blue))))
   `(gnus-cite-10 ((t (:foreground ,zenburn-MB-yellow-1))))
   `(gnus-cite-11 ((t (:foreground ,zenburn-MB-yellow))))
   `(gnus-cite-2 ((t (:foreground ,zenburn-MB-blue-1))))
   `(gnus-cite-3 ((t (:foreground ,zenburn-MB-blue-2))))
   `(gnus-cite-4 ((t (:foreground ,zenburn-MB-green+2))))
   `(gnus-cite-5 ((t (:foreground ,zenburn-MB-green+1))))
   `(gnus-cite-6 ((t (:foreground ,zenburn-MB-green))))
   `(gnus-cite-7 ((t (:foreground ,zenburn-MB-red))))
   `(gnus-cite-8 ((t (:foreground ,zenburn-MB-red-1))))
   `(gnus-cite-9 ((t (:foreground ,zenburn-MB-red-2))))
   `(gnus-group-news-1-empty ((t (:foreground ,zenburn-MB-yellow))))
   `(gnus-group-news-2-empty ((t (:foreground ,zenburn-MB-green+3))))
   `(gnus-group-news-3-empty ((t (:foreground ,zenburn-MB-green+1))))
   `(gnus-group-news-4-empty ((t (:foreground ,zenburn-MB-blue-2))))
   `(gnus-group-news-5-empty ((t (:foreground ,zenburn-MB-blue-3))))
   `(gnus-group-news-6-empty ((t (:foreground ,zenburn-MB-bg+2))))
   `(gnus-group-news-low-empty ((t (:foreground ,zenburn-MB-bg+2))))
   `(gnus-signature ((t (:foreground ,zenburn-MB-yellow))))
   `(gnus-x ((t (:background ,zenburn-MB-fg :foreground ,zenburn-MB-bg))))
;;;;; guide-key
   `(guide-key/highlight-command-face ((t (:foreground ,zenburn-MB-blue))))
   `(guide-key/key-face ((t (:foreground ,zenburn-MB-green))))
   `(guide-key/prefix-command-face ((t (:foreground ,zenburn-MB-green+1))))
;;;;; helm
   `(helm-header
     ((t (:foreground ,zenburn-MB-green
                      :background ,zenburn-MB-bg
                      :underline nil
                      :box nil))))
   `(helm-source-header
     ((t (:foreground ,zenburn-MB-yellow
                      :background ,zenburn-MB-bg-1
                      :underline nil
                      :weight bold
                      :box (:line-width -1 :style released-button)))))
   `(helm-selection ((t (:background ,zenburn-MB-bg+1 :underline nil))))
   `(helm-selection-line ((t (:background ,zenburn-MB-bg+1))))
   `(helm-visible-mark ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-yellow-2))))
   `(helm-candidate-number ((t (:foreground ,zenburn-MB-green+4 :background ,zenburn-MB-bg-1))))
   `(helm-separator ((t (:foreground ,zenburn-MB-red :background ,zenburn-MB-bg))))
   `(helm-time-zone-current ((t (:foreground ,zenburn-MB-green+2 :background ,zenburn-MB-bg))))
   `(helm-time-zone-home ((t (:foreground ,zenburn-MB-red :background ,zenburn-MB-bg))))
   `(helm-bookmark-addressbook ((t (:foreground ,zenburn-MB-orange :background ,zenburn-MB-bg))))
   `(helm-bookmark-directory ((t (:foreground nil :background nil :inherit helm-ff-directory))))
   `(helm-bookmark-file ((t (:foreground nil :background nil :inherit helm-ff-file))))
   `(helm-bookmark-gnus ((t (:foreground ,zenburn-MB-magenta :background ,zenburn-MB-bg))))
   `(helm-bookmark-info ((t (:foreground ,zenburn-MB-green+2 :background ,zenburn-MB-bg))))
   `(helm-bookmark-man ((t (:foreground ,zenburn-MB-yellow :background ,zenburn-MB-bg))))
   `(helm-bookmark-w3m ((t (:foreground ,zenburn-MB-magenta :background ,zenburn-MB-bg))))
   `(helm-buffer-not-saved ((t (:foreground ,zenburn-MB-red :background ,zenburn-MB-bg))))
   `(helm-buffer-process ((t (:foreground ,zenburn-MB-cyan :background ,zenburn-MB-bg))))
   `(helm-buffer-saved-out ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg))))
   `(helm-buffer-size ((t (:foreground ,zenburn-MB-fg-1 :background ,zenburn-MB-bg))))
   `(helm-ff-directory ((t (:foreground ,zenburn-MB-cyan :background ,zenburn-MB-bg :weight bold))))
   `(helm-ff-file ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg :weight normal))))
   `(helm-ff-executable ((t (:foreground ,zenburn-MB-green+2 :background ,zenburn-MB-bg :weight normal))))
   `(helm-ff-invalid-symlink ((t (:foreground ,zenburn-MB-red :background ,zenburn-MB-bg :weight bold))))
   `(helm-ff-symlink ((t (:foreground ,zenburn-MB-yellow :background ,zenburn-MB-bg :weight bold))))
   `(helm-ff-prefix ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-yellow :weight normal))))
   `(helm-grep-cmd-line ((t (:foreground ,zenburn-MB-cyan :background ,zenburn-MB-bg))))
   `(helm-grep-file ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg))))
   `(helm-grep-finish ((t (:foreground ,zenburn-MB-green+2 :background ,zenburn-MB-bg))))
   `(helm-grep-lineno ((t (:foreground ,zenburn-MB-fg-1 :background ,zenburn-MB-bg))))
   `(helm-grep-match ((t (:foreground nil :background nil :inherit helm-match))))
   `(helm-grep-running ((t (:foreground ,zenburn-MB-red :background ,zenburn-MB-bg))))
   `(helm-moccur-buffer ((t (:foreground ,zenburn-MB-cyan :background ,zenburn-MB-bg))))
   `(helm-mu-contacts-address-face ((t (:foreground ,zenburn-MB-fg-1 :background ,zenburn-MB-bg))))
   `(helm-mu-contacts-name-face ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg))))
;;;;; helm-swoop
   `(helm-swoop-target-line-face ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg+1))))
   `(helm-swoop-target-word-face ((t (:foreground ,zenburn-MB-yellow :background ,zenburn-MB-bg+2 :weight bold))))
;;;;; hl-line-mode
   `(hl-line-face ((,class (:background ,zenburn-MB-bg-05))
                   (t :weight bold)))
   `(hl-line ((,class (:background ,zenburn-MB-bg-05)) ; old emacsen
              (t :weight bold)))
;;;;; hl-sexp
   `(hl-sexp-face ((,class (:background ,zenburn-MB-bg+1))
                   (t :weight bold)))
;;;;; ido-mode
   `(ido-first-match ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(ido-only-match ((t (:foreground ,zenburn-MB-orange :weight bold))))
   `(ido-subdir ((t (:foreground ,zenburn-MB-yellow))))
   `(ido-indicator ((t (:foreground ,zenburn-MB-yellow :background ,zenburn-MB-red-4))))
;;;;; iedit-mode
   `(iedit-occurrence ((t (:background ,zenburn-MB-bg+2 :weight bold))))
;;;;; jabber-mode
   `(jabber-roster-user-away ((t (:foreground ,zenburn-MB-green-1))))
   `(jabber-roster-user-online ((t (:foreground ,zenburn-MB-blue-1))))
   `(jabber-roster-user-dnd ((t (:foreground ,zenburn-MB-red+1))))
   `(jabber-rare-time-face ((t (:foreground ,zenburn-MB-green+1))))
   `(jabber-chat-prompt-local ((t (:foreground ,zenburn-MB-blue-1))))
   `(jabber-chat-prompt-foreign ((t (:foreground ,zenburn-MB-red+1))))
   `(jabber-activity-face((t (:foreground ,zenburn-MB-red+1))))
   `(jabber-activity-personal-face ((t (:foreground ,zenburn-MB-blue+1))))
   `(jabber-title-small ((t (:height 1.1 :weight bold))))
   `(jabber-title-medium ((t (:height 1.2 :weight bold))))
   `(jabber-title-large ((t (:height 1.3 :weight bold))))
;;;;; js2-mode
   `(js2-warning ((t (:underline ,zenburn-MB-orange))))
   `(js2-error ((t (:foreground ,zenburn-MB-red :weight bold))))
   `(js2-jsdoc-tag ((t (:foreground ,zenburn-MB-green-1))))
   `(js2-jsdoc-type ((t (:foreground ,zenburn-MB-green+2))))
   `(js2-jsdoc-value ((t (:foreground ,zenburn-MB-green+3))))
   `(js2-function-param ((t (:foreground, zenburn-MB-green+3))))
   `(js2-external-variable ((t (:foreground ,zenburn-MB-orange))))
;;;;; ledger-mode
   `(ledger-font-payee-uncleared-face ((t (:foreground ,zenburn-MB-red-1 :weight bold))))
   `(ledger-font-payee-cleared-face ((t (:foreground ,zenburn-MB-fg :weight normal))))
   `(ledger-font-xact-highlight-face ((t (:background ,zenburn-MB-bg+1))))
   `(ledger-font-pending-face ((t (:foreground ,zenburn-MB-orange weight: normal))))
   `(ledger-font-other-face ((t (:foreground ,zenburn-MB-fg))))
   `(ledger-font-posting-account-face ((t (:foreground ,zenburn-MB-blue-1))))
   `(ledger-font-posting-account-cleared-face ((t (:foreground ,zenburn-MB-fg))))
   `(ledger-font-posting-account-pending-face ((t (:foreground ,zenburn-MB-orange))))
   `(ledger-font-posting-amount-face ((t (:foreground ,zenburn-MB-orange))))
   `(ledger-occur-narrowed-face ((t (:foreground ,zenburn-MB-fg-1 :invisible t))))
   `(ledger-occur-xact-face ((t (:background ,zenburn-MB-bg+1))))
   `(ledger-font-comment-face ((t (:foreground ,zenburn-MB-green))))
   `(ledger-font-reconciler-uncleared-face ((t (:foreground ,zenburn-MB-red-1 :weight bold))))
   `(ledger-font-reconciler-cleared-face ((t (:foreground ,zenburn-MB-fg :weight normal))))
   `(ledger-font-reconciler-pending-face ((t (:foreground ,zenburn-MB-orange :weight normal))))
   `(ledger-font-report-clickable-face ((t (:foreground ,zenburn-MB-orange :weight normal))))
;;;;; linum-mode
   `(linum ((t (:foreground ,zenburn-MB-bg+1 :background ,zenburn-MB-bg))))
;;;;; macrostep
   `(macrostep-gensym-1
     ((t (:foreground ,zenburn-MB-green+2 :background ,zenburn-MB-bg-1))))
   `(macrostep-gensym-2
     ((t (:foreground ,zenburn-MB-red+1 :background ,zenburn-MB-bg-1))))
   `(macrostep-gensym-3
     ((t (:foreground ,zenburn-MB-blue+1 :background ,zenburn-MB-bg-1))))
   `(macrostep-gensym-4
     ((t (:foreground ,zenburn-MB-magenta :background ,zenburn-MB-bg-1))))
   `(macrostep-gensym-5
     ((t (:foreground ,zenburn-MB-yellow :background ,zenburn-MB-bg-1))))
   `(macrostep-expansion-highlight-face
     ((t (:inherit highlight))))
   `(macrostep-macro-face
     ((t (:underline t))))
;;;;; makefile
   `(makefile-space
     ((t (:inherit font-lock-comment-face))))
;;;;; magit
   `(magit-item-highlight ((t (:background ,zenburn-MB-bg+05))))
   `(magit-section-title ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(magit-process-ok ((t (:foreground ,zenburn-MB-green :weight bold))))
   `(magit-process-ng ((t (:foreground ,zenburn-MB-red :weight bold))))
   `(magit-branch ((t (:foreground ,zenburn-MB-blue :weight bold))))
   `(magit-log-author ((t (:foreground ,zenburn-MB-orange))))
   `(magit-log-sha1 ((t (:foreground, zenburn-MB-orange))))
;;;;; message-mode
   `(message-cited-text ((t (:inherit font-lock-comment-face))))
   `(message-header-name ((t (:foreground ,zenburn-MB-green+1))))
   `(message-header-other ((t (:foreground ,zenburn-MB-green))))
   `(message-header-to ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(message-header-from ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(message-header-cc ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(message-header-newsgroups ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(message-header-subject ((t (:foreground ,zenburn-MB-orange :weight bold))))
   `(message-header-xheader ((t (:foreground ,zenburn-MB-green))))
   `(message-mml ((t (:foreground ,zenburn-MB-yellow :weight bold))))
   `(message-separator ((t (:inherit font-lock-comment-face))))
;;;;; mew
   `(mew-face-header-subject ((t (:foreground ,zenburn-MB-orange))))
   `(mew-face-header-from ((t (:foreground ,zenburn-MB-yellow))))
   `(mew-face-header-date ((t (:foreground ,zenburn-MB-green))))
   `(mew-face-header-to ((t (:foreground ,zenburn-MB-red))))
   `(mew-face-header-key ((t (:foreground ,zenburn-MB-green))))
   `(mew-face-header-private ((t (:foreground ,zenburn-MB-green))))
   `(mew-face-header-important ((t (:foreground ,zenburn-MB-blue))))
   `(mew-face-header-marginal ((t (:foreground ,zenburn-MB-fg :weight bold))))
   `(mew-face-header-warning ((t (:foreground ,zenburn-MB-red))))
   `(mew-face-header-xmew ((t (:foreground ,zenburn-MB-green))))
   `(mew-face-header-xmew-bad ((t (:foreground ,zenburn-MB-red))))
   `(mew-face-body-url ((t (:foreground ,zenburn-MB-orange))))
   `(mew-face-body-comment ((t (:foreground ,zenburn-MB-fg :slant italic))))
   `(mew-face-body-cite1 ((t (:foreground ,zenburn-MB-green))))
   `(mew-face-body-cite2 ((t (:foreground ,zenburn-MB-blue))))
   `(mew-face-body-cite3 ((t (:foreground ,zenburn-MB-orange))))
   `(mew-face-body-cite4 ((t (:foreground ,zenburn-MB-yellow))))
   `(mew-face-body-cite5 ((t (:foreground ,zenburn-MB-red))))
   `(mew-face-mark-review ((t (:foreground ,zenburn-MB-blue))))
   `(mew-face-mark-escape ((t (:foreground ,zenburn-MB-green))))
   `(mew-face-mark-delete ((t (:foreground ,zenburn-MB-red))))
   `(mew-face-mark-unlink ((t (:foreground ,zenburn-MB-yellow))))
   `(mew-face-mark-refile ((t (:foreground ,zenburn-MB-green))))
   `(mew-face-mark-unread ((t (:foreground ,zenburn-MB-red-2))))
   `(mew-face-eof-message ((t (:foreground ,zenburn-MB-green))))
   `(mew-face-eof-part ((t (:foreground ,zenburn-MB-yellow))))
;;;;; mic-paren
   `(paren-face-match ((t (:foreground ,zenburn-MB-cyan :background ,zenburn-MB-bg :weight bold))))
   `(paren-face-mismatch ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-magenta :weight bold))))
   `(paren-face-no-match ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-red :weight bold))))
;;;;; mingus
   `(mingus-directory-face ((t (:foreground ,zenburn-MB-blue))))
   `(mingus-pausing-face ((t (:foreground ,zenburn-MB-magenta))))
   `(mingus-playing-face ((t (:foreground ,zenburn-MB-cyan))))
   `(mingus-playlist-face ((t (:foreground ,zenburn-MB-cyan ))))
   `(mingus-song-file-face ((t (:foreground ,zenburn-MB-yellow))))
   `(mingus-stopped-face ((t (:foreground ,zenburn-MB-red))))
;;;;; nav
   `(nav-face-heading ((t (:foreground ,zenburn-MB-yellow))))
   `(nav-face-button-num ((t (:foreground ,zenburn-MB-cyan))))
   `(nav-face-dir ((t (:foreground ,zenburn-MB-green))))
   `(nav-face-hdir ((t (:foreground ,zenburn-MB-red))))
   `(nav-face-file ((t (:foreground ,zenburn-MB-fg))))
   `(nav-face-hfile ((t (:foreground ,zenburn-MB-red-4))))
;;;;; mu4e
   `(mu4e-cited-1-face ((t (:foreground ,zenburn-MB-blue    :slant italic))))
   `(mu4e-cited-2-face ((t (:foreground ,zenburn-MB-green+2 :slant italic))))
   `(mu4e-cited-3-face ((t (:foreground ,zenburn-MB-blue-2  :slant italic))))
   `(mu4e-cited-4-face ((t (:foreground ,zenburn-MB-green   :slant italic))))
   `(mu4e-cited-5-face ((t (:foreground ,zenburn-MB-blue-4  :slant italic))))
   `(mu4e-cited-6-face ((t (:foreground ,zenburn-MB-green-1 :slant italic))))
   `(mu4e-cited-7-face ((t (:foreground ,zenburn-MB-blue    :slant italic))))
   `(mu4e-replied-face ((t (:foreground ,zenburn-MB-bg+3))))
   `(mu4e-trashed-face ((t (:foreground ,zenburn-MB-bg+3 :strike-through t))))
;;;;; mumamo
   `(mumamo-background-chunk-major ((t (:background nil))))
   `(mumamo-background-chunk-submode1 ((t (:background ,zenburn-MB-bg-1))))
   `(mumamo-background-chunk-submode2 ((t (:background ,zenburn-MB-bg+2))))
   `(mumamo-background-chunk-submode3 ((t (:background ,zenburn-MB-bg+3))))
   `(mumamo-background-chunk-submode4 ((t (:background ,zenburn-MB-bg+1))))
;;;;; org-mode
   `(org-agenda-date-today
     ((t (:foreground, zenburn-MB-green :underline t))) t)
   `(org-agenda-structure
     ((t (:inherit font-lock-comment-face))))
   `(org-archived ((t (:foreground ,zenburn-MB-fg :weight bold))))
   `(org-checkbox ((t (:background ,zenburn-MB-bg+2 :foreground ,zenburn-MB-fg+1
                                   :box (:line-width 1 :style released-button)))))
   `(org-date ((t (:foreground ,zenburn-MB-blue :underline t))))
   `(org-deadline-announce ((t (:foreground ,zenburn-MB-red-1))))
   `(org-done ((t (:bold t :weight bold :foreground ,zenburn-MB-green+3))))
   `(org-formula ((t (:foreground ,zenburn-MB-yellow-2))))
   `(org-headline-done ((t (:foreground ,zenburn-MB-green+3))))
   ;;;;;;`(org-hide ((t (:foreground ,zenburn-MB-bg-1))))
   `(org-hide ((t (:foreground "#4e4e4e"))))
   `(org-level-1 ((t (:foreground ,zenburn-MB-orange))))
   `(org-level-2 ((t (:foreground ,zenburn-MB-green+4))))
   `(org-level-3 ((t (:foreground ,zenburn-MB-blue-1))))
   `(org-level-4 ((t (:foreground ,zenburn-MB-yellow-2))))
   `(org-level-5 ((t (:foreground ,zenburn-MB-cyan))))
   `(org-level-6 ((t (:foreground ,zenburn-MB-green+2))))
   `(org-level-7 ((t (:foreground ,zenburn-MB-red-4))))
   `(org-level-8 ((t (:foreground ,zenburn-MB-blue-4))))
   `(org-link ((t (:foreground ,zenburn-MB-yellow-2 :underline t))))
   `(org-scheduled ((t (:foreground ,zenburn-MB-green+4))))
   `(org-scheduled-previously ((t (:foreground ,zenburn-MB-red))))
   `(org-scheduled-today ((t (:foreground ,zenburn-MB-blue+1))))
   `(org-sexp-date ((t (:foreground ,zenburn-MB-blue+1 :underline t))))
   `(org-special-keyword ((t (:inherit font-lock-comment-face))))
   `(org-table ((t (:foreground ,zenburn-MB-green+2))))
   `(org-tag ((t (:bold t :weight bold))))
   `(org-time-grid ((t (:foreground ,zenburn-MB-orange))))
   `(org-todo ((t (:bold t :foreground ,zenburn-MB-red :weight bold))))
   `(org-upcoming-deadline ((t (:inherit font-lock-keyword-face))))
   `(org-warning ((t (:bold t :foreground ,zenburn-MB-red :weight bold :underline nil))))
   `(org-column ((t (:background ,zenburn-MB-bg-1))))
   `(org-column-title ((t (:background ,zenburn-MB-bg-1 :underline t :weight bold))))
   `(org-mode-line-clock ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg-1))))
   `(org-mode-line-clock-overrun ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-red-1))))
   `(org-ellipsis ((t (:foreground ,zenburn-MB-yellow-1 :underline t))))
   `(org-footnote ((t (:foreground ,zenburn-MB-cyan :underline t))))
;;;;; outline
   `(outline-1 ((t (:foreground ,zenburn-MB-orange))))
   `(outline-2 ((t (:foreground ,zenburn-MB-green+4))))
   `(outline-3 ((t (:foreground ,zenburn-MB-blue-1))))
   `(outline-4 ((t (:foreground ,zenburn-MB-yellow-2))))
   `(outline-5 ((t (:foreground ,zenburn-MB-cyan))))
   `(outline-6 ((t (:foreground ,zenburn-MB-green+2))))
   `(outline-7 ((t (:foreground ,zenburn-MB-red-4))))
   `(outline-8 ((t (:foreground ,zenburn-MB-blue-4))))
;;;;; p4
   `(p4-depot-added-face ((t :inherit diff-added)))
   `(p4-depot-branch-op-face ((t :inherit diff-changed)))
   `(p4-depot-deleted-face ((t :inherit diff-removed)))
   `(p4-depot-unmapped-face ((t :inherit diff-changed)))
   `(p4-diff-change-face ((t :inherit diff-changed)))
   `(p4-diff-del-face ((t :inherit diff-removed)))
   `(p4-diff-file-face ((t :inherit diff-file-header)))
   `(p4-diff-head-face ((t :inherit diff-header)))
   `(p4-diff-ins-face ((t :inherit diff-added)))
;;;;; perspective
   `(persp-selected-face ((t (:foreground ,zenburn-MB-yellow-2 :inherit mode-line))))
;;;;; powerline
   `(powerline-active1 ((t (:background ,zenburn-MB-bg-05 :inherit mode-line))))
   `(powerline-active2 ((t (:background ,zenburn-MB-bg+2 :inherit mode-line))))
   `(powerline-inactive1 ((t (:background ,zenburn-MB-bg+1 :inherit mode-line-inactive))))
   `(powerline-inactive2 ((t (:background ,zenburn-MB-bg+3 :inherit mode-line-inactive))))
;;;;; proofgeneral
   `(proof-active-area-face ((t (:underline t))))
   `(proof-boring-face ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-bg+2))))
   `(proof-command-mouse-highlight-face ((t (:inherit proof-mouse-highlight-face))))
   `(proof-debug-message-face ((t (:inherit proof-boring-face))))
   `(proof-declaration-name-face ((t (:inherit font-lock-keyword-face :foreground nil))))
   `(proof-eager-annotation-face ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-orange))))
   `(proof-error-face ((t (:foreground ,zenburn-MB-fg :background ,zenburn-MB-red-4))))
   `(proof-highlight-dependency-face ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-yellow-1))))
   `(proof-highlight-dependent-face ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-orange))))
   `(proof-locked-face ((t (:background ,zenburn-MB-blue-5))))
   `(proof-mouse-highlight-face ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-orange))))
   `(proof-queue-face ((t (:background ,zenburn-MB-red-4))))
   `(proof-region-mouse-highlight-face ((t (:inherit proof-mouse-highlight-face))))
   `(proof-script-highlight-error-face ((t (:background ,zenburn-MB-red-2))))
   `(proof-tacticals-name-face ((t (:inherit font-lock-constant-face :foreground nil :background ,zenburn-MB-bg))))
   `(proof-tactics-name-face ((t (:inherit font-lock-constant-face :foreground nil :background ,zenburn-MB-bg))))
   `(proof-warning-face ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-yellow-1))))
;;;;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face ((t (:foreground ,zenburn-MB-fg))))
   `(rainbow-delimiters-depth-2-face ((t (:foreground ,zenburn-MB-green+4))))
   `(rainbow-delimiters-depth-3-face ((t (:foreground ,zenburn-MB-yellow-2))))
   `(rainbow-delimiters-depth-4-face ((t (:foreground ,zenburn-MB-cyan))))
   `(rainbow-delimiters-depth-5-face ((t (:foreground ,zenburn-MB-green+2))))
   `(rainbow-delimiters-depth-6-face ((t (:foreground ,zenburn-MB-blue+1))))
   `(rainbow-delimiters-depth-7-face ((t (:foreground ,zenburn-MB-yellow-1))))
   `(rainbow-delimiters-depth-8-face ((t (:foreground ,zenburn-MB-green+1))))
   `(rainbow-delimiters-depth-9-face ((t (:foreground ,zenburn-MB-blue-2))))
   `(rainbow-delimiters-depth-10-face ((t (:foreground ,zenburn-MB-orange))))
   `(rainbow-delimiters-depth-11-face ((t (:foreground ,zenburn-MB-green))))
   `(rainbow-delimiters-depth-12-face ((t (:foreground ,zenburn-MB-blue-5))))
;;;;; rcirc
   `(rcirc-my-nick ((t (:foreground ,zenburn-MB-blue))))
   `(rcirc-other-nick ((t (:foreground ,zenburn-MB-orange))))
   `(rcirc-bright-nick ((t (:foreground ,zenburn-MB-blue+1))))
   `(rcirc-dim-nick ((t (:foreground ,zenburn-MB-blue-2))))
   `(rcirc-server ((t (:foreground ,zenburn-MB-green))))
   `(rcirc-server-prefix ((t (:foreground ,zenburn-MB-green+1))))
   `(rcirc-timestamp ((t (:foreground ,zenburn-MB-green+2))))
   `(rcirc-nick-in-message ((t (:foreground ,zenburn-MB-yellow))))
   `(rcirc-nick-in-message-full-line ((t (:bold t))))
   `(rcirc-prompt ((t (:foreground ,zenburn-MB-yellow :bold t))))
   `(rcirc-track-nick ((t (:inverse-video t))))
   `(rcirc-track-keyword ((t (:bold t))))
   `(rcirc-url ((t (:bold t))))
   `(rcirc-keyword ((t (:foreground ,zenburn-MB-yellow :bold t))))
;;;;; rpm-mode
   `(rpm-spec-dir-face ((t (:foreground ,zenburn-MB-green))))
   `(rpm-spec-doc-face ((t (:foreground ,zenburn-MB-green))))
   `(rpm-spec-ghost-face ((t (:foreground ,zenburn-MB-red))))
   `(rpm-spec-macro-face ((t (:foreground ,zenburn-MB-yellow))))
   `(rpm-spec-obsolete-tag-face ((t (:foreground ,zenburn-MB-red))))
   `(rpm-spec-package-face ((t (:foreground ,zenburn-MB-red))))
   `(rpm-spec-section-face ((t (:foreground ,zenburn-MB-yellow))))
   `(rpm-spec-tag-face ((t (:foreground ,zenburn-MB-blue))))
   `(rpm-spec-var-face ((t (:foreground ,zenburn-MB-red))))
;;;;; rst-mode
   `(rst-level-1-face ((t (:foreground ,zenburn-MB-orange))))
   `(rst-level-2-face ((t (:foreground ,zenburn-MB-green+1))))
   `(rst-level-3-face ((t (:foreground ,zenburn-MB-blue-1))))
   `(rst-level-4-face ((t (:foreground ,zenburn-MB-yellow-2))))
   `(rst-level-5-face ((t (:foreground ,zenburn-MB-cyan))))
   `(rst-level-6-face ((t (:foreground ,zenburn-MB-green-1))))
;;;;; sh-mode
   `(sh-heredoc     ((t (:foreground ,zenburn-MB-yellow :bold t))))
   `(sh-quoted-exec ((t (:foreground ,zenburn-MB-red))))
;;;;; show-paren
   `(show-paren-mismatch ((t (:foreground ,zenburn-MB-red+1 :background ,zenburn-MB-bg+3 :weight bold))))
   `(show-paren-match ((t (:background ,zenburn-MB-bg+3 :weight bold))))
;;;;; smartparens
   `(sp-show-pair-mismatch-face ((t (:foreground ,zenburn-MB-red+1 :background ,zenburn-MB-bg+3 :weight bold))))
   `(sp-show-pair-match-face ((t (:background ,zenburn-MB-bg+3 :weight bold))))
;;;;; sml-mode-line
   '(sml-modeline-end-face ((t :inherit default :width condensed)))
;;;;; SLIME
   `(slime-repl-output-face ((t (:foreground ,zenburn-MB-red))))
   `(slime-repl-inputed-output-face ((t (:foreground ,zenburn-MB-green))))
   `(slime-error-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-red)))
      (t
       (:underline ,zenburn-MB-red))))
   `(slime-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-orange)))
      (t
       (:underline ,zenburn-MB-orange))))
   `(slime-style-warning-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-yellow)))
      (t
       (:underline ,zenburn-MB-yellow))))
   `(slime-note-face
     ((((supports :underline (:style wave)))
       (:underline (:style wave :color ,zenburn-MB-green)))
      (t
       (:underline ,zenburn-MB-green))))
   `(slime-highlight-face ((t (:inherit highlight))))
;;;;; speedbar
   `(speedbar-button-face ((t (:foreground ,zenburn-MB-green+2))))
   `(speedbar-directory-face ((t (:foreground ,zenburn-MB-cyan))))
   `(speedbar-file-face ((t (:foreground ,zenburn-MB-fg))))
   `(speedbar-highlight-face ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-green+2))))
   `(speedbar-selected-face ((t (:foreground ,zenburn-MB-red))))
   `(speedbar-separator-face ((t (:foreground ,zenburn-MB-bg :background ,zenburn-MB-blue-1))))
   `(speedbar-tag-face ((t (:foreground ,zenburn-MB-yellow))))
;;;;; tabbar
   `(tabbar-button ((t (:foreground ,zenburn-MB-fg
                                    :background ,zenburn-MB-bg))))
   `(tabbar-selected ((t (:foreground ,zenburn-MB-fg
                                      :background ,zenburn-MB-bg
                                      :box (:line-width -1 :style pressed-button)))))
   `(tabbar-unselected ((t (:foreground ,zenburn-MB-fg
                                        :background ,zenburn-MB-bg+1
                                        :box (:line-width -1 :style released-button)))))
;;;;; term
   `(term-color-black ((t (:foreground ,zenburn-MB-bg
                                       :background ,zenburn-MB-bg-1))))
   `(term-color-red ((t (:foreground ,zenburn-MB-red-2
                                       :background ,zenburn-MB-red-4))))
   `(term-color-green ((t (:foreground ,zenburn-MB-green
                                       :background ,zenburn-MB-green+2))))
   `(term-color-yellow ((t (:foreground ,zenburn-MB-orange
                                       :background ,zenburn-MB-yellow))))
   `(term-color-blue ((t (:foreground ,zenburn-MB-blue-1
                                      :background ,zenburn-MB-blue-4))))
   `(term-color-magenta ((t (:foreground ,zenburn-MB-magenta
                                         :background ,zenburn-MB-red))))
   `(term-color-cyan ((t (:foreground ,zenburn-MB-cyan
                                       :background ,zenburn-MB-blue))))
   `(term-color-white ((t (:foreground ,zenburn-MB-fg
                                       :background ,zenburn-MB-fg-1))))
   '(term-default-fg-color ((t (:inherit term-color-white))))
   '(term-default-bg-color ((t (:inherit term-color-black))))
;;;;; undo-tree
   `(undo-tree-visualizer-active-branch-face ((t (:foreground ,zenburn-MB-fg+1 :weight bold))))
   `(undo-tree-visualizer-current-face ((t (:foreground ,zenburn-MB-red-1 :weight bold))))
   `(undo-tree-visualizer-default-face ((t (:foreground ,zenburn-MB-fg))))
   `(undo-tree-visualizer-register-face ((t (:foreground ,zenburn-MB-yellow))))
   `(undo-tree-visualizer-unmodified-face ((t (:foreground ,zenburn-MB-cyan))))
;;;;; volatile-highlights
   `(vhl/default-face ((t (:background ,zenburn-MB-bg-05))))
;;;;; web-mode
   `(web-mode-builtin-face ((t (:inherit ,font-lock-builtin-face))))
   `(web-mode-comment-face ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-constant-face ((t (:inherit ,font-lock-constant-face))))
   `(web-mode-css-at-rule-face ((t (:foreground ,zenburn-MB-orange ))))
   `(web-mode-css-prop-face ((t (:foreground ,zenburn-MB-orange))))
   `(web-mode-css-pseudo-class-face ((t (:foreground ,zenburn-MB-green+3 :weight bold))))
   `(web-mode-css-rule-face ((t (:foreground ,zenburn-MB-blue))))
   `(web-mode-doctype-face ((t (:inherit ,font-lock-comment-face))))
   `(web-mode-folded-face ((t (:underline t))))
   `(web-mode-function-name-face ((t (:foreground ,zenburn-MB-blue))))
   `(web-mode-html-attr-name-face ((t (:foreground ,zenburn-MB-orange))))
   `(web-mode-html-attr-value-face ((t (:inherit ,font-lock-string-face))))
   `(web-mode-html-tag-face ((t (:foreground ,zenburn-MB-cyan))))
   `(web-mode-keyword-face ((t (:inherit ,font-lock-keyword-face))))
   `(web-mode-preprocessor-face ((t (:inherit ,font-lock-preprocessor-face))))
   `(web-mode-string-face ((t (:inherit ,font-lock-string-face))))
   `(web-mode-type-face ((t (:inherit ,font-lock-type-face))))
   `(web-mode-variable-name-face ((t (:inherit ,font-lock-variable-name-face))))
   `(web-mode-server-background-face ((t (:background ,zenburn-MB-bg))))
   `(web-mode-server-comment-face ((t (:inherit web-mode-comment-face))))
   `(web-mode-server-string-face ((t (:inherit web-mode-string-face))))
   `(web-mode-symbol-face ((t (:inherit font-lock-constant-face))))
   `(web-mode-warning-face ((t (:inherit font-lock-warning-face))))
   `(web-mode-whitespaces-face ((t (:background ,zenburn-MB-red))))
;;;;; whitespace-mode
   `(whitespace-space ((t (:background ,zenburn-MB-bg+1 :foreground ,zenburn-MB-bg+1))))
   `(whitespace-hspace ((t (:background ,zenburn-MB-bg+1 :foreground ,zenburn-MB-bg+1))))
   `(whitespace-tab ((t (:background ,zenburn-MB-red-1))))
   `(whitespace-newline ((t (:foreground ,zenburn-MB-bg+1))))
   `(whitespace-trailing ((t (:background ,zenburn-MB-red))))
   `(whitespace-line ((t (:background ,zenburn-MB-bg :foreground ,zenburn-MB-magenta))))
   `(whitespace-space-before-tab ((t (:background ,zenburn-MB-orange :foreground ,zenburn-MB-orange))))
   `(whitespace-indentation ((t (:background ,zenburn-MB-yellow :foreground ,zenburn-MB-red))))
   `(whitespace-empty ((t (:background ,zenburn-MB-yellow))))
   `(whitespace-space-after-tab ((t (:background ,zenburn-MB-yellow :foreground ,zenburn-MB-red))))
;;;;; wanderlust
   `(wl-highlight-folder-few-face ((t (:foreground ,zenburn-MB-red-2))))
   `(wl-highlight-folder-many-face ((t (:foreground ,zenburn-MB-red-1))))
   `(wl-highlight-folder-path-face ((t (:foreground ,zenburn-MB-orange))))
   `(wl-highlight-folder-unread-face ((t (:foreground ,zenburn-MB-blue))))
   `(wl-highlight-folder-zero-face ((t (:foreground ,zenburn-MB-fg))))
   `(wl-highlight-folder-unknown-face ((t (:foreground ,zenburn-MB-blue))))
   `(wl-highlight-message-citation-header ((t (:foreground ,zenburn-MB-red-1))))
   `(wl-highlight-message-cited-text-1 ((t (:foreground ,zenburn-MB-red))))
   `(wl-highlight-message-cited-text-2 ((t (:foreground ,zenburn-MB-green+2))))
   `(wl-highlight-message-cited-text-3 ((t (:foreground ,zenburn-MB-blue))))
   `(wl-highlight-message-cited-text-4 ((t (:foreground ,zenburn-MB-blue+1))))
   `(wl-highlight-message-header-contents-face ((t (:foreground ,zenburn-MB-green))))
   `(wl-highlight-message-headers-face ((t (:foreground ,zenburn-MB-red+1))))
   `(wl-highlight-message-important-header-contents ((t (:foreground ,zenburn-MB-green+2))))
   `(wl-highlight-message-header-contents ((t (:foreground ,zenburn-MB-green+1))))
   `(wl-highlight-message-important-header-contents2 ((t (:foreground ,zenburn-MB-green+2))))
   `(wl-highlight-message-signature ((t (:foreground ,zenburn-MB-green))))
   `(wl-highlight-message-unimportant-header-contents ((t (:foreground ,zenburn-MB-fg))))
   `(wl-highlight-summary-answered-face ((t (:foreground ,zenburn-MB-blue))))
   `(wl-highlight-summary-disposed-face ((t (:foreground ,zenburn-MB-fg
                                                         :slant italic))))
   `(wl-highlight-summary-new-face ((t (:foreground ,zenburn-MB-blue))))
   `(wl-highlight-summary-normal-face ((t (:foreground ,zenburn-MB-fg))))
   `(wl-highlight-summary-thread-top-face ((t (:foreground ,zenburn-MB-yellow))))
   `(wl-highlight-thread-indent-face ((t (:foreground ,zenburn-MB-magenta))))
   `(wl-highlight-summary-refiled-face ((t (:foreground ,zenburn-MB-fg))))
   `(wl-highlight-summary-displaying-face ((t (:underline t :weight bold))))
;;;;; which-func-mode
   `(which-func ((t (:foreground ,zenburn-MB-green+4))))
;;;;; yascroll
   `(yascroll:thumb-text-area ((t (:background ,zenburn-MB-bg-1))))
   `(yascroll:thumb-fringe ((t (:background ,zenburn-MB-bg-1 :foreground ,zenburn-MB-bg-1))))
   ))

;;; Theme Variables
(zenburn-MB-with-color-variables
  (custom-theme-set-variables
   'zenburn-MB
;;;;; ansi-color
   `(ansi-color-names-vector [,zenburn-MB-bg ,zenburn-MB-red ,zenburn-MB-green ,zenburn-MB-yellow
                                          ,zenburn-MB-blue ,zenburn-MB-magenta ,zenburn-MB-cyan ,zenburn-MB-fg])
;;;;; fill-column-indicator
   `(fci-rule-color ,zenburn-MB-bg-05)
;;;;; vc-annotate
   `(vc-annotate-color-map
     '(( 20. . ,zenburn-MB-red-1)
       ( 40. . ,zenburn-MB-red)
       ( 60. . ,zenburn-MB-orange)
       ( 80. . ,zenburn-MB-yellow-2)
       (100. . ,zenburn-MB-yellow-1)
       (120. . ,zenburn-MB-yellow)
       (140. . ,zenburn-MB-green-1)
       (160. . ,zenburn-MB-green)
       (180. . ,zenburn-MB-green+1)
       (200. . ,zenburn-MB-green+2)
       (220. . ,zenburn-MB-green+3)
       (240. . ,zenburn-MB-green+4)
       (260. . ,zenburn-MB-cyan)
       (280. . ,zenburn-MB-blue-2)
       (300. . ,zenburn-MB-blue-1)
       (320. . ,zenburn-MB-blue)
       (340. . ,zenburn-MB-blue+1)
       (360. . ,zenburn-MB-magenta)))
   `(vc-annotate-very-old-color ,zenburn-MB-magenta)
   `(vc-annotate-background ,zenburn-MB-bg-1)
   ))

;;; Rainbow Support

(declare-function rainbow-mode 'rainbow-mode)
(declare-function rainbow-colorize-by-assoc 'rainbow-mode)

(defvar zenburn-MB-add-font-lock-keywords nil
  "Whether to add font-lock keywords for zenburn-MB color names.
In buffers visiting library `zenburn-MB-theme.el' the zenburn-MB
specific keywords are always added.  In all other Emacs-Lisp
buffers this variable controls whether this should be done.
This requires library `rainbow-mode'.")

(defvar zenburn-MB-colors-font-lock-keywords nil)

;; (defadvice rainbow-turn-on (after zenburn-MB activate)
;;   "Maybe also add font-lock keywords for zenburn-MB colors."
;;   (when (and (derived-mode-p 'emacs-lisp-mode)
;;              (or zenburn-MB-add-font-lock-keywords
;;                  (equal (file-name-nondirectory (buffer-file-name))
;;                         "zenburn-MB-theme.el")))
;;     (unless zenburn-MB-colors-font-lock-keywords
;;       (setq zenburn-MB-colors-font-lock-keywords
;;             `((,(regexp-opt (mapcar 'car zenburn-MB-colors-alist) 'words)
;;                (0 (rainbow-colorize-by-assoc zenburn-MB-colors-alist))))))
;;     (font-lock-add-keywords nil zenburn-MB-colors-font-lock-keywords)))

;; (defadvice rainbow-turn-off (after zenburn-MB activate)
;;   "Also remove font-lock keywords for zenburn-MB colors."
;;   (font-lock-remove-keywords nil zenburn-MB-colors-font-lock-keywords))

;;; Footer

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'zenburn-MB)

;; Local Variables:
;; no-byte-compile: t
;; indent-tabs-mode: nil
;; eval: (when (require 'rainbow-mode nil t) (rainbow-mode 1))
;; End:
;;; zenburn-MB-theme.el ends here
