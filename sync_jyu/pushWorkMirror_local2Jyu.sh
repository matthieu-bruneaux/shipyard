#! /bin/bash

### * Description


# Synchronize the remote directory ~/work in Jyu with the local 
# filesystem (recursively) by pushing the content of the local
# ~/work directory to the homologous remote directory
# mirror: remote files absent from the local directory are deleted

### * Parameters

logFile=~/.syncLog
sourceDir=/home/matthieu/work
destDir=/home/mabrunea

### * Run

# rsync [options] source dest
echo "Rsync from miniJulius to Jyu"
logLine=$(date)$'\t'"Full work sync - Rsync $sourceDir to $syncDest"
echo $logLine >> $logFile

rsync -aiz $@ --progress --delete -e "ssh mabrunea@halava.cc.jyu.fi ssh" $sourceDir mabrunea@130.234.109.113:$destDir

# options
# a: archive mode (recursive mode, preserve symbolic links, permissions,
#    timestamp and owner and group)
# v: verbose
# z: enable compression
# u: do not overwrite the modified file at the destination
# progress: visualise the progress for each file
# delete: delete the files at destination if not present at source
