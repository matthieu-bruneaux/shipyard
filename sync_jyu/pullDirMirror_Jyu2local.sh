#! /bin/bash

### * Description

# Synchronize the current directory with the corresponding remote directory in
# Jyu (pull from Jyu to local)

# 2019-04-09:
# Since there were some modifications on jalava filesystem which makes the use
# of ssh keys on jalava to connect to my JyU workstation impractical, I use
# agent forwarding from my laptop with the -A option.
# See:
# - https://developer.github.com/v3/guides/using-ssh-agent-forwarding/
# - http://toddharris.net/blog/2005/10/23/rsyncing-through-an-ssh-tunnel/

### * Parameters

logFile=~/.syncLog
backupDate=$(date)
syncDest=`pwd`
syncOrigin="/home/mabrunea"
syncOrigin=$syncOrigin${syncDest#/home/matthieu}
syncDest=`dirname $syncDest`

# http://stackoverflow.com/questions/5311956/bash-remove-first-directory-component-from-variable-path-of-file
# http://unix.stackexchange.com/questions/28771/how-to-remove-last-part-of-a-path-in-bash

### * Run

# rsync [options] source dest
printf "\033[0;32mRsync from Jyu \033[1;32m($syncOrigin)\033[0;32m to miniJulius \033[1;34m($syncDest)\033[0m\n"
logLine=$(date)$'\t'"Jyu to local sync - Rsync $sourceDir to $syncDest"
echo $logLine >> $logFile

sleep 2
rsync -aiz $@ --progress --delete -e "ssh -xA mabrunea@jalava.cc.jyu.fi ssh -x" mabrunea@130.234.109.113:$syncOrigin $syncDest

# options
# a: archive mode (recursive mode, preserve symbolic links, permissions,
#    timestamp and owner and group)
# v: verbose
# z: enable compression
# u: do not overwrite the modified file at the destination
# progress: visualise the progress for each file
# delete: delete the files at destination if not present at source
