#! /bin/bash

# description
# -----------

# Synchronize the remote directory ~/work in Jyu with the local 
# filesystem (recursively) by pulling the content of the remote
# ~/work directory to the homologous local directory
# mirror: remote files absent from the remote directory are deleted

### * Parameters

logFile=~/.syncLog
sourceDir=/home/mabrunea/work
destDir=/home/matthieu

### * Run

# rsync [options] source dest
echo "Rsync from Jyu to miniJulius"
logLine=$(date)$'\t'"Full work sync - Rsync $sourceDir to $syncDest"
echo $logLine >> $logFile

rsync -aiz $@ --progress --delete -e "ssh mabrunea@halava.cc.jyu.fi ssh" mabrunea@130.234.109.113:$sourceDir $destDir

# options
# a: archive mode (recursive mode, preserve symbolic links, permissions,
#    timestamp and owner and group)
# v: verbose
# z: enable compression
# u: do not overwrite the modified file at the destination
# progress: visualise the progress for each file
# delete: delete the files at destination if not present at source
