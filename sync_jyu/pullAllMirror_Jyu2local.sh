#! /bin/bash

### * Description

# Synchronize all the directories of interest between the remote computer and
# the local computer

### * Parameters

logFile=~/.syncLog
backupDate=$(date)

### * Run

echo "Rsync from Jyu to miniJulius"
echo "Pull (mirror) :"
echo "  - work directory"
echo "  - documentation directory"
echo "  - org directory"
echo "  - programming directory"
logLine=$(date)$'\t'"Jyu to local sync - Rsync work, documentation, org and programming"
echo $logLine >> $logFile

cd ~/work; pullDirMirror_Jyu2local.sh $@
#pullWorkMirror_Jyu2local.sh $@
cd ~/documentation; pullDirMirror_Jyu2local.sh $@
cd ~/org; pullDirMirror_Jyu2local.sh $@
cd ~/programming; pullDirMirror_Jyu2local.sh $@
