#! /bin/bash

### * Description

# Synchronize the current directory with the corresponding remote directory in
# Jyu (push from local to Jyu)

# 2019-04-09:
# Since there were some modifications on jalava filesystem which makes the use
# of ssh keys on jalava to connect to my JyU workstation impractical, I use
# agent forwarding from my laptop with the -A option.
# See:
# - https://developer.github.com/v3/guides/using-ssh-agent-forwarding/
# - http://toddharris.net/blog/2005/10/23/rsyncing-through-an-ssh-tunnel/

### * Parameters

logFile=~/.syncLog
backupDate=$(date)
syncDest="/home/mabrunea"
syncOrigin=`pwd`
syncDest=`dirname $syncDest${syncOrigin#/home/matthieu}`

# http://stackoverflow.com/questions/5311956/bash-remove-first-directory-component-from-variable-path-of-file
# http://unix.stackexchange.com/questions/28771/how-to-remove-last-part-of-a-path-in-bash

### * Run

# rsync [options] source dest
printf "\033[0;34mRsync from miniJulius \033[1;34m($syncOrigin)\033[0;34m to Jyu \033[1;32m($syncDest)\033[0m\n"
logLine=$(date)$'\t'"Local to Jyu sync - Rsync $sourceDir to $syncDest"
echo $logLine >> $logFile

sleep 4
rsync -aiz $@ --progress --delete -e "ssh -xA mabrunea@halava.cc.jyu.fi ssh -x " $syncOrigin mabrunea@130.234.109.113:$syncDest

# options
# a: archive mode (recursive mode, preserve symbolic links, permissions,
#    timestamp and owner and group)
# v: verbose
# z: enable compression
# u: do not overwrite the modified file at the destination
# progress: visualise the progress for each file
# delete: delete the files at destination if not present at source

