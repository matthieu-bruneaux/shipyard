#! /bin/bash

### * Description

# Synchronize all the directories of interest between the local computer and
# the remote computer

### * Parameters

logFile=~/.syncLog
backupDate=$(date)

### * Run

echo "Rsync from miniJulius to Jyu"
echo "Push (mirror) :"
echo "  - work directory"
echo "  - documentation directory"
echo "  - org directory"
echo "  - programming directory"
logLine=$(date)$'\t'"Local to Jyu sync - Rsync work, documentation, org and programming"
echo $logLine >> $logFile

sleep 4
cd ~/work; pushDirMirror_local2Jyu.sh $@
#pushWorkMirror_local2Jyu.sh $@
cd ~/documentation; pushDirMirror_local2Jyu.sh $@
cd ~/org; pushDirMirror_local2Jyu.sh $@
cd ~/programming; pushDirMirror_local2Jyu.sh $@
