#! /bin/bash

upower -d | grep percentage | cut -d: -f2 | sed -e 's/ *//'
