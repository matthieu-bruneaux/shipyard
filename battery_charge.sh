#! /bin/bash

upower -d | grep percentage | head -1 | cut -d: -f2 | tr -d [:blank:]
